﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TireMarket.Startup))]
namespace TireMarket
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
